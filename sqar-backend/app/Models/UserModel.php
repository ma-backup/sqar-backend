<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserModel extends Model
{
    use HasFactory;

    public static function CheckPassword($empNum, $password){
        return DB::connection('sqlsrvgblaccess')
                ->table('tblEnrolled_List')
                ->where('Software_Name', 'SQAR')
                ->where('empNum', $empNum)                
                ->where('password', $password)
                ->count();
    }
    public static function ChangePassword($empNum, $password){
        return DB::connection('sqlsrvgblaccess')
            ->table('tblEnrolled_List')
            ->where('Software_Name', 'SQAR')
            ->where('empNum', $empNum)
            ->update(['password' => $password]);
    }
}
