<?php

namespace App\Http\Controllers\Api;

use DB;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SqarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function countSqar(){
        $result = DB::select('SELECT STUFF(SQAR_Control_Number, 1, 7, :empty) as SQAR_COUNTS FROM tblSQAR_Process WHERE ID = (SELECT MAX(ID) FROM tblSQAR_Process)',
        ['empty' => '']);

        return response()->json($result);
    }

    public function getSqars(){
        $result = DB::select(
            'SELECT [ID]
              ,[SQAR_Control_Number]
              ,[SQAR_PO_Number]
              ,[SQAR_Invoice_DR]
              ,[SQAR_Invoice_Qty]
              ,[SQAR_Supplier_Name]
              ,[SQAR_No_Issued]
              ,[SQAR_Maker_Name]
              ,[SQAR_Response_Month]
              ,[SQAR_Received_Date]
              ,[SQAR_Contact_Person]
              ,[SQAR_Part_Name]
              ,[SQAR_Part_Number]
              ,[SQAR_Date_Sent]
              ,[SQAR_Response_Time]
              ,[SQAR_Response_Due_Date]
              ,[SQAR_Model]
              ,[SQAR_Rejected_Qty]
              ,[SQAR_Deffects]
              ,[SQAR_Rejected_Rate]
              ,[SQAR_Remarks]
              ,[SQAR_Affected_Qty]
              ,[SQAR_Affected_Lot_No]
              ,[SQAR_Trouble_Classification]
              ,[SQAR_Prepared_By]
              ,[SQAR_Prepared_Date]
              ,[SQAR_QC_Supervisor]
              ,[SQAR_QC_Prep_Date]
              ,[SQAR_IQC_Sr_Supervisor]
              ,[SQAR_IQC_Prep_Date]
              ,[SQAR_QC_SH]
              ,[SQAR_QC_SH_Prep_Date]
              ,[SQAR_Asst_Mgr]
              ,[SQAR_Asst_Mgr_Date]
              ,[SQAR_Plant_Mgr]
              ,[SQAR_Plant_Mgr_Date]
              ,[SQAR_Defect_Rank]
              ,[SQAR_Affected_Lot]
              ,[SQAR_Prod_Sample_Size]
              ,[SQAR_Prod_Input_Qty]
              ,[SQAR_Deffect_Occurence]
              ,[SQAR_Email_Status]
              ,[SQAR_Status]
              ,[SQAR_Read_Status]
              ,[SQAR_Defective_Mat]
              ,[SQAR_NG_MAT_DispOSAL]
              ,[SQAR_Sample_Size]
              ,[SQAR_Input_QTY]
              ,[SQAR_Response_Due_Date_RCAA]
              ,[SQAR_NG_Amount]
              ,[SQAR_Tech_Supp_Mngr]
              ,[SQAR_Tech_Supp_Mngr_Signed_Date]
              ,[SQAR_Asst_Factory_Mngr]
              ,[SQAR_Asst_Factory_Mngr_Signed_Date]
              ,[SQAR_Factory_Mngr]
              ,[SQAR_Factory_Mngr_Signed_Date]
              ,[SQAR_Defects_Qty]
              ,[SQAR_SupMatDispo_IQC_Sr_Supvr_Approval]
              ,[SQAR_SupMatDispo_IQC_Sr_Supvr_Date_Approved]
              ,[SQAR_SupMatDispo_QC_Sec_Head_Approval]
              ,[SQAR_SupMatDispo_QC_Sec_Head_Date_Approved]
              ,[SQAR_SupMatDispo_QC_Asst_Mangr_Approval]
              ,[SQAR_SupMatDispo_QC_Asst_Mangr_Date_Approved]
              ,[SQAR_MAT_RCCA_Comments_Request]
              ,[SQAR_RCCA_Disposition_isApproved]
              ,[SQAR_RCCA_Response_Date]
              ,[SQAR_SupRCCA_IQC_Sr_Supvr_Approval]
              ,[SQAR_SupRCCA_IQC_Sr_Supvr_Date_Approved]
              ,[SQAR_SupRCCA_QC_Sec_Head_Approval]
              ,[SQAR_SupRCCA_QC_Sec_Head_Date_Approved]
              ,[SQAR_SupRCCA_QC_Asst_Mangr_Approval]
              ,[SQAR_SupRCCA_QC_Asst_Mangr_Date_Approved]
              ,[SQAR_First_Delivery_Date]
              ,[SQAR_First_Delivery_Qty]
              ,[SQAR_First_Delivery_isOK]
              ,[SQAR_Second_Delivery_Date]
              ,[SQAR_Second_Delivery_Qty]
              ,[SQAR_Second_Delivery_isOK]
              ,[SQAR_Third_Delivery_Date]
              ,[SQAR_Third_Delivery_Qty]
              ,[SQAR_Third_Delivery_isOK]
              ,[SQAR_Include_Supp_Tech_Mngr]
              ,[SQAR_Affected_Lot_IsRTV]
              ,[SQAR_Affected_Lot_IsOkToUse]
              ,[SQAR_MatCodeBoxSeqId]
              ,[SQAR_Defect_Info_No]
              ,[SQAR_General_Status]
              ,[SQAR_Cancelled_By]
              ,[SQAR_Cancel_Date]
              ,[SQAR_Cancel_Remarks]
              ,ROW_NUMBER() OVER(ORDER BY SQAR_Prepared_Date DESC) as ROW_NUMBER 
            FROM tblSQAR_Process
            WHERE [IsDeleted] <> 1
            ORDER BY SQAR_Prepared_Date DESC'
        );
        return response()->json($result);
    }

    public function addSqar(Request $sqarData){
        $result = DB::insert('
            INSERT INTO tblSQAR_Process
                   (SQAR_Control_Number
                   ,SQAR_PO_Number
                   ,SQAR_Invoice_DR
                   ,SQAR_Invoice_Qty
                   ,SQAR_Supplier_Name
                   ,SQAR_No_Issued
                   ,SQAR_Maker_Name
                   ,SQAR_Response_Month
                   ,SQAR_Received_Date
                   ,SQAR_Contact_Person
                   ,SQAR_Deffect_Occurence
                   ,SQAR_Part_Name
                   ,SQAR_Part_Number
                   ,SQAR_Model
                   ,SQAR_Rejected_Qty
                   ,SQAR_Deffects
                   ,SQAR_Rejected_Rate
                   ,SQAR_Remarks
                   ,SQAR_Affected_Qty
                   ,SQAR_Affected_Lot_No
                   ,SQAR_Trouble_Classification
                   ,SQAR_Prepared_By
                   ,SQAR_Prepared_Date
                   ,SQAR_Defect_Rank
                   ,SQAR_Affected_Lot
                   ,SQAR_Status
                   ,SQAR_Defective_Mat
                   ,SQAR_NG_MAT_DispOSAL
                   ,SQAR_Sample_Size
                   ,SQAR_Input_QTY
                   ,SQAR_NG_Amount
                   ,SQAR_Defects_Qty
                   ,SQAR_Include_Supp_Tech_Mngr
                   ,SQAR_Affected_Lot_IsRTV
                   ,SQAR_Affected_Lot_IsOkToUse
                   ,SQAR_MatCodeBoxSeqId
                   ,SQAR_Defect_Info_No
                   ,SQAR_General_Status)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, GETDATE(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                    [$sqarData->SQAR_Control_Number,
                    $sqarData->SQAR_PO_Number,
                    $sqarData->SQAR_Invoice_DR,
                    $sqarData->SQAR_Invoice_Qty,
                    $sqarData->SQAR_Supplier_Name,
                    $sqarData->SQAR_No_Issued,
                    $sqarData->SQAR_Maker_Name,
                    $sqarData->SQAR_Response_Month,
                    $sqarData->SQAR_Received_Date,
                    $sqarData->SQAR_Contact_Person,
                    $sqarData->SQAR_Deffect_Occurence,
                    $sqarData->SQAR_Part_Name,
                    $sqarData->SQAR_Part_Number,
                    $sqarData->SQAR_Model,
                    $sqarData->SQAR_Rejected_Qty,
                    $sqarData->SQAR_Deffects,
                    $sqarData->SQAR_Rejected_Rate,
                    $sqarData->SQAR_Remarks,
                    $sqarData->SQAR_Affected_Qty,
                    $sqarData->SQAR_Affected_Lot_No,
                    $sqarData->SQAR_Trouble_Classification,
                    $sqarData->SQAR_Prepared_By,
                    $sqarData->SQAR_Defect_Rank,
                    $sqarData->SQAR_Affected_Lot,
                    $sqarData->SQAR_Status,
                    $sqarData->SQAR_Defective_Mat,
                    $sqarData->SQAR_NG_MAT_DispOSAL,
                    $sqarData->SQAR_Sample_Size,
                    $sqarData->SQAR_Input_QTY,
                    $sqarData->SQAR_NG_Amount,
                    $sqarData->SQAR_Defects_Qty,
                    $sqarData->SQAR_Include_Supp_Tech_Mngr,
                    $sqarData->SQAR_Affected_Lot_IsRTV,
                    $sqarData->SQAR_Affected_Lot_IsOkToUse,
                    $sqarData->SQAR_MatCodeBoxSeqId,
                    $sqarData->SQAR_Defect_Info_No,
                    'OPEN']
        );
        return response()->json($result);
    }

    public function approveSqar(Request $request){
        $position = "";
        $dateSigned = "";
        date_default_timezone_set("Asia/Hong_Kong");
        $dateTime = date("Y-m-d H:i:s");
        if($request->position == "QC Supervisor" || $request->position == "IQC Supervisor"){
            $position = "SQAR_QC_Supervisor";
            $dateSigned = "SQAR_QC_Prep_Date";
        }else if($request->position == "IQC Sr. Supervisor"){
             $position = "SQAR_IQC_Sr_Supervisor";
             $dateSigned = "SQAR_IQC_Prep_Date";
        }else if($request->position == "QC Section Head"){
             $position = "SQAR_QC_SH";
             $dateSigned = "SQAR_QC_SH_Prep_Date";
        }else if($request->position == "QC Assistant Manager"){
            $position = "SQAR_Asst_Mgr";
            $dateSigned = "SQAR_Asst_Mgr_Date";
        }else if($request->position == "Supplier Technical Manager"){
            $position = "SQAR_Tech_Supp_Mngr";
            $dateSigned = "SQAR_Tech_Supp_Mngr_Signed_Date";
        }else if($request->position == "Assistant Factory Manager"){
            $position = "SQAR_Asst_Factory_Mngr";
            $dateSigned = "SQAR_Asst_Factory_Mngr_Signed_Date";
        }else if($request->position == "Factory Manager" || $request->position == "President"){
            $position = "SQAR_Factory_Mngr";
            $dateSigned = "SQAR_Factory_Mngr_Signed_Date";
        }else{
            return response()->json("Approval Unsuccessful");
        }
       $result = DB::table('tblSQAR_Process')
                    ->where('SQAR_Control_Number', $request->sqarControlNo)
                    ->update([$position => $request->fullName, $dateSigned => $dateTime]);

        return response()->json($result);
    }

    public function setDateSent(Request $request){
        $result = DB::table('tblSQAR_Process')
                    ->where('SQAR_Control_Number', $request->sqarControlNo)
                    ->update(['SQAR_Date_Sent' => $request->dateSent,
                                'SQAR_Status' => 3]);
        if($result == 1){
            $response = (object) ['success' => true];
        }else{
            $response = (object) ['success' => false];
        }

        return response()->json($response);
    }

    public function approveSupMatDispo(Request $request){
        $position = "";
        $dateSigned = "";
        date_default_timezone_set("Asia/Hong_Kong");
        $dateTime = date("Y-m-d H:i:s");

        if($request->position == "IQC Sr. Supervisor"){

             $position = "SQAR_SupMatDispo_IQC_Sr_Supvr_Approval";
             $dateSigned = "SQAR_SupMatDispo_IQC_Sr_Supvr_Date_Approved";

        }else if($request->position == "QC Section Head"){

             $position = "SQAR_SupMatDispo_QC_Sec_Head_Approval";
             $dateSigned = "SQAR_SupMatDispo_QC_Sec_Head_Date_Approved";

        }else if($request->position == "QC Assistant Manager"){

            $position = "SQAR_SupMatDispo_QC_Asst_Mangr_Approval";
            $dateSigned = "SQAR_SupMatDispo_QC_Asst_Mangr_Date_Approved";

            $updateSqarStatus = DB::table('tblSQAR_Process')
                                ->where('SQAR_Control_Number', $request->sqarControlNo)
                                ->update(['SQAR_Status' => 5]);
        }

        $result = DB::table('tblSQAR_Process')
                    ->where('SQAR_Control_Number', $request->sqarControlNo)
                    ->update([$position => $request->fullName, $dateSigned => $dateTime]);

        return response()->json($result);
    }

    public function approveSupplierRCCA(Request $request){
        $position = "";
        $dateSigned = "";
        date_default_timezone_set("Asia/Hong_Kong");
        $dateTime = date("Y-m-d H:i:s");

        if($request->position == "IQC Sr. Supervisor"){

             $position = "SQAR_SupRCCA_IQC_Sr_Supvr_Approval";
             $dateSigned = "SQAR_SupRCCA_IQC_Sr_Supvr_Date_Approved";

        }else if($request->position == "QC Section Head"){

             $position = "SQAR_SupRCCA_QC_Sec_Head_Approval";
             $dateSigned = "SQAR_SupRCCA_QC_Sec_Head_Date_Approved";

        }else if($request->position == "QC Assistant Manager"){

            $position = "SQAR_SupRCCA_QC_Asst_Mangr_Approval";
            $dateSigned = "SQAR_SupRCCA_QC_Asst_Mangr_Date_Approved";

            $result = DB::table('tblSQAR_Process')
                        ->where('SQAR_Control_Number', $request->sqarControlNo)
                        ->update(['SQAR_Status' => 8]);
        }

        $result = DB::table('tblSQAR_Process')
                    ->where('SQAR_Control_Number', $request->sqarControlNo)
                    ->update([$position => $request->fullName, $dateSigned => $dateTime]);
        return response()->json($result);
    }

    //UPDATE SQAR STATUS TO SENT
    public function updateSqarStatus(Request $request){

        $finalResult = 0;
        if($request->defectRank == 'B')
        {
            $result = DB::select('
                SELECT CASE                        
                        WHEN SQAR_Asst_Mgr IS NOT NULL OR SQAR_Asst_Mgr != :empty1
                            THEN CAST(1 AS BIT) 
                        ELSE 
                            CAST(0 AS BIT) END AS isFullyApproved
                FROM tblSQAR_Process
                WHERE SQAR_Control_Number = :id', 
                ['id' => $request->sqarControlNo, 'empty1' => ""]);

            $finalResult = $result[0]->isFullyApproved;

        }else{
            $result = DB::select('
                SELECT CASE                    
                        WHEN SQAR_Asst_Factory_Mngr IS NOT NULL OR SQAR_Asst_Factory_Mngr != :empty1
                            THEN CASE 
                                WHEN SQAR_Factory_Mngr IS NOT NULL OR SQAR_Factory_Mngr != :empty2
                                    THEN CAST(1 AS BIT)
                                ELSE 
                                    CAST(0 AS BIT) END
                        ELSE 
                            CAST(0 AS BIT) END AS isFullyApproved
                FROM tblSQAR_Process
                WHERE SQAR_Control_Number = :id', ['id' => $request->sqarControlNo, 'empty1' => "", 'empty2' => ""]); 

            $finalResult = $result[0]->isFullyApproved;
        }

        date_default_timezone_set("Asia/Hong_Kong");
        $dateTime = date("Y-m-d H:i:s");

        if($finalResult == 1){
            $approvalResult = DB::table('tblSQAR_Process')
                    ->where('SQAR_Control_Number', $request->sqarControlNo)
                    ->update(['SQAR_Status' => 3,
                    'SQAR_Date_Sent' => $dateTime,
                    'SQAR_Response_Due_Date' => date("Y-m-d", strtotime($request->supMatDisposalDueDate)),
                    'SQAR_Response_Due_Date_RCAA' => date("Y-m-d", strtotime($request->supRccaDueDate))]);
            if($approvalResult == 1){
                $updateSqarResult = (object) ['updateSqarResult' => true];
                return response()->json($updateSqarResult);
            }           
        }
        $updateFailed = (object) ['updateSqarResult' => false];
        return response()->json($updateFailed);
    }

    //UPDATE SQAR STATUS TO SUPPLIER MATERIAL DISPOSITION RECEIVED
    public function updateSqarStatusToSupMatDispoReceived(Request $request){
        $result = DB::table('tblSQAR_Process')
                    ->where('SQAR_Control_Number', $request->sqarControlNo)
                    ->update(['SQAR_Status' => 4]);
        if($result == 1){
            $updateStatusResult = (object) ['updateStatusResult' => true];
        }else{
            $updateStatusResult = (object) ['updateStatusResult' => false];
        }
        return response()->json($updateStatusResult);
    }

    public function updateImageAccessKey(Request $request){
         $result = DB::table('images')
                    ->where('id', $request->imageId)
                    ->update(['access_code' => $request->imageAccessKey]);

        return response()->json($result);
    }

    public function updateImageAccessKeyAndInfo(Request $request){
         $result = DB::table('images')
                    ->where('id', $request->imageId)
                    ->update(['access_code' => $request->imageAccessKey,
                                'title' => $request->title,
                                'description' => $request->description]);

        return response()->json($result);
    }

    public function getImageName(Request $request){
        $result = DB::select('SELECT [id] 
                                    ,[image] 
                                    ,[updated_at] 
                                    ,[created_at]
                                    ,[access_code]
                                    ,[title]
                                    ,[description]
                                FROM images
                                WHERE access_code = :imageAccessCode ORDER BY id',
                                ['imageAccessCode' => $request->imageAccessKey]);
        return response()->json($result);
    }

    public function updateSentDate(Request $request){
        date_default_timezone_set("Asia/Hong_Kong");
        $dateTime = date("Y-m-d H:i:s");

        $result = DB::table('tblSQAR_Process')
            ->where('SQAR_Control_Number', $request->sqarControlNo)
            ->update(['SQAR_Date_Sent' => $dateTime, 
                'SQAR_Response_Due_Date' => date('Y-m-d', strtotime("+ 2 days")),
                'SQAR_Response_Due_Date_RCAA' => date('Y-m-d', strtotime("+ 5 days"))]);
        return response()->json($result);
    }

    public function saveMatRccaComments(Request $request){
        $result = DB::table('tblSQAR_Process')
            ->where('SQAR_Control_Number', $request->sqarControlNo)
            ->update(['SQAR_MAT_RCCA_Comments_Request' => $request->matRccaComments]);
        return response()->json($result);
    }

    public function approveRccaDisposition(Request $request){
        $result = DB::table('tblSQAR_Process')
            ->where('SQAR_Control_Number', $request->sqarControlNo)
            ->update(['SQAR_RCCA_Disposition_isApproved' => true]);
        return response()->json($result);
    }

    public function disapproveRccaDisposition(Request $request){
        $result = DB::table('tblSQAR_Process')
            ->where('SQAR_Control_Number', $request->sqarControlNo)
            ->update(['SQAR_RCCA_Disposition_isApproved' => false]);
        return response()->json($result);
    }

    //UPDATE SQAR STATUS HERE ALONG WITH THE UPDATE OF THE RCCA RESPONSE DATE
    public function updateSupplierRccaResponseDate(Request $request){
        $result = DB::table('tblSQAR_Process')
            ->where('SQAR_Control_Number', $request->sqarControlNo)
            ->update(['SQAR_RCCA_Response_Date' => $request->rccaDateReceived, 'SQAR_Status' => 6]);
        return response()->json($result);
    }

    public function approveDelivery(Request $request){
        $columns = (object) [
            'columnOne' => 'SQAR_First_Delivery_Date',
            'columnTwo' => 'SQAR_First_Delivery_Qty',
            'columnThree' => 'SQAR_First_Delivery_isOK'
        ];

        if($request->number == 1){
            //UPDATE SQAR STATUS TO (9) FIRST DELIVERY APPROVED
            DB::table('tblSQAR_Process')
            ->where('SQAR_Control_Number', $request->sqarControlNo)
            ->update(['SQAR_Status' => 9]);
        }

        if($request->number == 2){

            $columns->columnOne = 'SQAR_Second_Delivery_Date';
            $columns->columnTwo = 'SQAR_Second_Delivery_Qty';
            $columns->columnThree = 'SQAR_Second_Delivery_isOK';

            //UPDATE SQAR STATUS TO (10) SECOND DELIVERY APPROVED
            DB::table('tblSQAR_Process')
            ->where('SQAR_Control_Number', $request->sqarControlNo)
            ->update(['SQAR_Status' => 11]);

        }else if($request->number == 3){

            $columns->columnOne = 'SQAR_Third_Delivery_Date';
            $columns->columnTwo = 'SQAR_Third_Delivery_Qty';
            $columns->columnThree = 'SQAR_Third_Delivery_isOK';

            //UPDATE SQAR STATUS TO (14) CLOSED AFTER LAST DELIVERY IS OK
            DB::table('tblSQAR_Process')
            ->where('SQAR_Control_Number', $request->sqarControlNo)
            ->update(['SQAR_Status' => 14, 'SQAR_General_Status' => 'CLOSED']);
        }

        $result = DB::table('tblSQAR_Process')
            ->where('SQAR_Control_Number', $request->sqarControlNo)
            ->update([$columns->columnOne => $request->deliveryDate,
                      $columns->columnTwo => $request->deliveryQuantity,
                      $columns->columnThree => 1]);
        return response()->json($result);
    }

    //SAVE DEFECTS TO TBLSQARDEFECTS
    public function saveDefects(Request $request){
        $result = DB::insert('
            INSERT INTO tblSQAR_Defects
                        (SQAR_Control_Number,
                        Defect,
                        Quantity)
            VALUES(?, ?, ?)',
            [$request->sqarControlNo,
             $request->defect,
             $request->quantity]);
        return response()->json($result);
    }

    //UPDATE RCCA DATE WHEN RCCA WAS ADDED ON DIFFERENT DAY IT WAS RECEIVED
    public function updateRCCAResponseDate(Request $request){
        $result = DB::table('tblSQAR_Process')
        ->where('SQAR_Control_Number', $request->sqarControlNo)
        ->update(['SQAR_RCCA_Response_Date' => $request->rccaDateReceived]);

        return response()->json($result);
    }

    //GET LAST SQAR FOR SQAR CONTROL NUMBER
    public function getLastSqarControlNumber(){
        $result = DB::select('SELECT TOP 1 SQAR_Control_Number FROM tblSQAR_Process ORDER BY ID desc');
        return response()->json($result);
    }

    //SQAR CANCELLATION
    public function cancelSqar(Request $request){
        //change general status to "CANCELLED". Set who cancelled, date cancelled, cancellation remarks
        $result = DB::table('tblSQAR_Process')
            ->where('SQAR_Control_Number', $request->sqarControlNo)
            ->update(['SQAR_General_Status' => 'CANCELLED', 
            'SQAR_Cancelled_By' => $request->cancelledBy, 
            'SQAR_Cancel_Date' => Carbon::today()->toDateString(),
            'SQAR_Cancel_Remarks' => $request->cancelRemarks]);

        return response()->json($result);        
    }

    public function getAll(){
        $result = DB::select('SELECT * FROM inspectiondata');
        return response()->json($result);
    }
}
