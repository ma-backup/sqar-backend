<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MS21_Duplicate extends Controller
{
    public function findAllPartName(Request $request){
        $result = DB::connection('sqlsrvms21')->select('SELECT DISTINCT [Goods name2] AS partName FROM POLISTOrigdownloadToss WHERE [Goods name] = :partNo', ["partNo" => $request->partNo]);
        return response()->json($result);
    }
}
