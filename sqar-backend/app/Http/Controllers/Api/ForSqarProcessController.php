<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ForSqarProcessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getForSqarProcess(){
        
        // $results = DB::select('SELECT A.supplier as SUPPLIER,
        //     A.partname as PART_NAME,
        //     A.part_number as PART_NO,
        //     A.maker as MAKER,
        //     A.invoicequant as INVOICE_QTY,
        //     A.invoicequant as AFFECTED_QTY,
        //     A.recieved_date as DATE_RECEIVED,
        //     A.inspected_date as DATE_INSPECTED,
        //     A.sample_size as SAMPLE_SIZE,
        //     A.invoice_no as INVOICE,
        //     A.material_type as MATERIAL_TYPE,
        //     A.MaterialCodeBoxSeqID as MATCODEBOX,
        //     A.PO_Number as PO_NUMBER,
        //     ROW_NUMBER() OVER(ORDER BY A.inspected_date DESC) as ROW_NUMBER

        //     FROM inspectiondata as A

        //     WHERE A.MaterialCodeBoxSeqID in (SELECT MaterialCodeBoxSeqID 
        //                                      FROM tblOverall_Judgement 
        //                                      WHERE (overall_judgement = :failed OR Production_Status = :failed2)
        //                                      AND MaterialCodeBoxSeqID IS NOT NULL 
        //                                      AND MaterialCodeBoxSeqID != :empty)
        //     AND A.invoice_no IS NOT NULL
        //     AND datalength(A.invoice_no) <> :zero',
        //     ['zero' => 0, 'failed' => "FAILED", 'failed2' => "FAILED", 'empty' => ""]);
        
        $results = DB::select('SELECT A.supplier as SUPPLIER,
            A.partname as PART_NAME,
            A.part_number as PART_NO,
            A.maker as MAKER,
            A.invoicequant as INVOICE_QTY,
            A.invoicequant as AFFECTED_QTY,
            A.recieved_date as DATE_RECEIVED,
            A.inspected_date as DATE_INSPECTED,
            A.sample_size as SAMPLE_SIZE,
            ISNULL(C.defectqty, 0) AS DEFECT_QTY,
			ISNULL(SAMPLE_SIZE, 0) - ISNULL(C.defectqty, 0) AS PASSED_QTY,
			CAST((100.00 * ISNULL(C.defectqty, 0) / ISNULL(SAMPLE_SIZE, 0)) AS DECIMAL(10, 2)) AS REJECT_RATE,
            A.invoice_no as INVOICE,
            A.material_type as MATERIAL_TYPE,
            A.MaterialCodeBoxSeqID as MATCODEBOX,
            A.PO_Number as PO_NUMBER,
			B.overall_judgement as OVERALL_JUDGEMENT,
			B.Production_Status as PRODUCTION_STATUS,
            D.SQAR_Control_Number as SQAR_CONTROL_NO,
            ROW_NUMBER() OVER(ORDER BY A.inspected_date DESC) as ROW_NUMBER

            FROM inspectiondata as A
            
            INNER JOIN tblOverall_Judgement as B ON A.MaterialCodeBoxSeqID = B.MaterialCodeBoxSeqID

            LEFT JOIN
                (
                    SELECT SUM(defectqty) AS defectqty, MaterialCodeBoxSeqID
                    FROM
                        (
                            SELECT defectqty, MaterialCodeBoxSeqID from Appearance_Inspection
                            UNION ALL
                            SELECT defectqty, MaterialCodeBoxSeqID from DimensionalCheck
                            UNION ALL
                            SELECT defectqty, MaterialCodeBoxSeqID from FunctionalCheck
                        ) AS DefectQty GROUP BY MaterialCodeBoxSeqID
                ) C ON A.MaterialCodeBoxSeqID = C.MaterialCodeBoxSeqID
            
            LEFT JOIN tblSQAR_Process D ON A.MaterialCodeBoxSeqID = D.SQAR_MatCodeBoxSeqId AND D.IsDeleted <> 1

            WHERE (B.overall_judgement = :failed OR B.Production_Status = :failed2)
                AND B.MaterialCodeBoxSeqID IS NOT NULL 
                AND B.MaterialCodeBoxSeqID != :empty
                AND A.invoice_no IS NOT NULL
                AND datalength(A.invoice_no) <> :zero',
            ['zero' => 0, 'failed' => "FAILED", 'failed2' => "FAILED", 'empty' => ""]);

        return response()->json($results);
    }

    public function getAffectedLotNumbers(Request $request){
        $result = DB::select('SELECT DISTINCT lot_no 
                FROM LotNumber 
                WHERE MaterialCodeBoxSeqID = :matcodebox', 
                ['matcodebox' => $request->matCodeBoxSeqId]);

        $lotNos = [];
        foreach($result as $value){
            array_push($lotNos, $value->lot_no);
        }
        return response()->json($lotNos);
    }

    public function getRejects(Request $request){
        $result1 = DB::select('SELECT SUM(defectqty) as DEFECT_QTY, defect_enc as DEFECT
            FROM Appearance_Inspection
            WHERE invoice_no = :invoice
            AND MaterialCodeBoxSeqID = :matcodebox GROUP BY defect_enc',
            ['invoice' => $request->INVOICE, 'matcodebox' => $request->MATCODEBOX]);

        $result2 = DB::select('SELECT SUM(defectqty) as DEFECT_QTY, defect_enc as DEFECT
            FROM DimensionalCheck
            WHERE invoice_no = :invoice
            AND MaterialCodeBoxSeqID = :matcodebox GROUP BY defect_enc',
            ['invoice' => $request->INVOICE, 'matcodebox' => $request->MATCODEBOX]);

        $result3 = DB::select('SELECT SUM(defectqty) as DEFECT_QTY, defect_enc as DEFECT
            FROM FunctionalCheck
            WHERE invoice_no = :invoice
            AND MaterialCodeBoxSeqID = :matcodebox GROUP BY defect_enc',
            ['invoice' => $request->INVOICE, 'matcodebox' => $request->MATCODEBOX]);

        return  response()->json([$result1, $result2, $result3]);
    }

    public function getMajorDefects(){

        $pcbDefects = DB::select('SELECT [Deffect_Definition] AS PCB_DEFECT
                                FROM [IQCDatabase].[dbo].[tblMaterial_NG_Definition]
                                WHERE Material_Type = :pcb AND Type_Deffect = :major', 
                                ['pcb' => 'PCB', 'major' => 'MAJOR']);
          
        $plasticDefects = DB::select('SELECT [Deffect_Definition] AS PLASTIC_DEFECT
                                    FROM [IQCDatabase].[dbo].[tblMaterial_NG_Definition]
                                    WHERE Material_Type = :plastic AND Type_Deffect = :major',
                                    ['plastic' => 'PLASTIC', 'major' => 'MAJOR']);
          
        $metalDefects = DB::select('SELECT [Deffect_Definition] AS METAL_DEFECT
                                    FROM [IQCDatabase].[dbo].[tblMaterial_NG_Definition]
                                    WHERE Material_Type = :metal AND Type_Deffect = :major',
                                    ['metal' => 'METAL', 'major' => 'MAJOR']);

        $pcbDefectsArr = [];
        $plasticDefectsArr = [];
        $metalDefectsArr = [];

        foreach($pcbDefects as $value){
            array_push($pcbDefectsArr, $value->PCB_DEFECT);
        }

        foreach($plasticDefects as $value){
            array_push($plasticDefectsArr, $value->PLASTIC_DEFECT);
        }

        foreach($metalDefects as $value){
            array_push($metalDefectsArr, $value->METAL_DEFECT);
        }

        $majorDefects = (object) array(
            'PCB_MAJOR_DEFECTS' => $pcbDefectsArr,
            'PLASTIC_MAJOR_DEFECTS' => $plasticDefectsArr,
            'METAL_MAJOR_DEFECTS' => $metalDefectsArr
        );

        return response()->json($majorDefects);
    }

    public function getSqarStatusDefinition(){
        $result = DB::select('SELECT [SQAR_Status_No]
                                    ,[SQAR_Status]
                            FROM [IQCDatabase].[dbo].[tblSQAR_Status]');
        return response()->json($result);
    }

    public function getTechSuppMngrSupplier(){
        $result = DB::select('SELECT [id], [Supplier] FROM [IQCDatabase].[dbo].[tblSQAR_Tech_Supp_Mngr_Supplier]');
        $supplier = [];
        foreach ($result as $value) {
            array_push($supplier, $value->Supplier);
        }
        return response()->json($supplier);
    }

    public function checkCriticalNGCheckPoint(Request $request){
        $result = DB::select("SELECT COUNT('Id') AS CriticalCheckPointNGCount
        FROM DimensionalCheck
        WHERE IsCriticalCheckPoint = :one
        AND (defect_enc IS NOT NULL OR defect_enc != :blank1)
        AND (defectqty IS NOT NULL OR defectqty != :blank2)
        AND invoice_no = :invoice
        AND MaterialCodeBoxSeqID = :matcodebox", 
        ['one' => 1, 'blank1' => '', 'blank2' => '', 'invoice' => $request->INVOICE, 'matcodebox' => $request->MATCODEBOX]);

        return response()->json($result);        
    }
}
