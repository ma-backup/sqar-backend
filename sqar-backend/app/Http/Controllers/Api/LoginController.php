<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function Login(Request $request)
    {
        $result = DB::connection('sqlsrvgblaccess')->select(
            'SELECT Admin, Position, Software_Name, isActive, Full_Name, empNum
             FROM tblEnrolled_List
             WHERE empNum = :empNum AND password = :password AND isActive = 1 AND Software_Name = :sqar',
             ['empNum' => $request->empNum, 'password' => $request->password, 'sqar' => 'SQAR']
        );
        return response()->json($result);
    }
}
