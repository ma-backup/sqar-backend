<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\File;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

class FileController extends Controller
{
    public function storeFile(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
              'file' => 'required|mimes:doc,docx,pdf,txt,csv,xlsx|max:20000',
        ]);   
 
        if($validator->fails()) {  
            return response()->json(['success'=>false, 'error'=>$validator->errors()], 401);                        
         }  
  
        if ($file = $request->file('file')) {
            $path = $file->store('public/files');
            $name = $file->getClientOriginalName();
 
            //store your file into directory and db
            $save = new File();
            $save->name = $file;
            $save->store_path= $path;
            $save->save();
              
            return response()->json([
                "success" => true,
                "message" => "File successfully uploaded",
                "file" => $save
            ]);
  
        }
    }

    public function retrieveFile($path)
    {
        $file = Storage::get($path);
        return response($file, 200)->header('Content-Type', Storage::mimeType($path));
    }

    public function updateSupportDocument(Request $request)
    {
        $result = DB::table('files')
            ->where('id', $request->id)
            ->update(['name' => $request->fileName, 
                'description' => $request->fileDescription, 
                'sqar_control_no' => $request->sqarControlNo]);
        return response()->json($result);
    }

    public function getFileInformation(Request $request)
    {
        $result = DB::table('files')->where('sqar_control_no', $request->sqarControlNo)->get();
        return response()->json($result);
    }
}
