<?php

namespace App\Http\Controllers\Api2;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LotNumberController extends Controller
{

    public function find(Request $request){
        $result = DB::select(
            'SELECT [id]
            ,[invoice_no]
            ,[part_no]
            ,[part_name]
            ,[total_quantity]
            ,[quantity_recieved]
            ,[lot_no]
            ,[lot_quantity]
            ,[box_number]
            ,[reject]
            ,[sample_size]
            ,[goodsCode]
            ,[MaterialCodeBoxSeqID]
            ,[remarks]
            ,[Date]
            ,[KEN_ID]
            ,[DIFF]
            ,[Total_good]
            ,[PO_Number]
            FROM LotNumber
            WHERE MaterialCodeBoxSeqID = :matCodeBox
            ORDER BY id',
            ['matCodeBox' => $request->matCodeBox]
        );
        return response()->json($result);
    }

    public function findAll(Request $request){
        $result = DB::select(
            'SELECT [invoice_no]
            ,[part_no]
            ,[part_name]
            ,[total_quantity]
            ,[quantity_recieved]
            ,[lot_no]
            ,[lot_quantity]
            ,[box_number]
            ,[reject]
            ,[sample_size]
            ,[goodsCode]
            ,[PO_Number]
        FROM [IQCDatabase].[dbo].[LotNumber]
        WHERE invoice_no = :invoice
        ORDER BY part_no',
        ['invoice' => $request->invoice]
        );

        // $invoiceLots = [];
        // $partLot = array(
        //     'partNo' => '',
        //     'partName' => '',
        //     'totalQuantity' => 0,
        //     'lotQuantityTotal' => 0,
        //     'lots' => []
        // );
        
        // $samePartNoToPrevLot = true;
        // $lotQuantityTotal = 0;

        // for($i = 0; $i < count($result); $i++){
        //     if($i != 0){
        //         if(str_replace(' ', '', $result[$i]->part_no) == str_replace(' ', '', $result[$i - 1]->part_no)){
        //             $samePartNoToPrevLot = true;
        //         }else{
        //             $samePartNoToPrevLot = false;
        //         }
        //     }

        //     $lot = (object)[
        //         'lotNo' => $result[$i]->lot_no,
        //         'PONo' => $result[$i]->PO_Number,
        //         'lotQty' => $result[$i]->lot_quantity,
        //         'boxNo' => $result[$i]->box_number,
        //         'reject' => $result[$i]->reject,
        //         'sampleSize' => $result[$i]->sample_size
        //     ];

        //     if($i !=  count($result) -1){
        //         if($samePartNoToPrevLot){
        //             $lotQuantityTotal += $result[$i]->lot_quantity;
        //             array_push($partLot['lots'], $lot);
        //         }else{
        //             $partLot['partNo'] = $result[$i-1]->part_no;
        //             $partLot['partName'] = $result[$i-1]->part_name;
        //             $partLot['totalQuantity'] = $result[$i-1]->total_quantity;
        //             $partLot['lotQuantityTotal'] = $lotQuantityTotal;
        //             array_push($invoiceLots, $partLot);
        //             $partLot['lots'] = [];
        //             $partLot['partNo'] = $result[$i]->part_no;
        //             $partLot['partName'] = $result[$i]->part_name;
        //             $partLot['totalQuantity'] = $result[$i]->total_quantity;
        //             $lotQuantityTotal = $result[$i]->lot_quantity;
        //             $partLot['lotQuantityTotal'] = $lotQuantityTotal;
        //             array_push($partLot['lots'], $lot);
        //         }
        //     }else{
        //         if($samePartNoToPrevLot){
        //             $lotQuantityTotal += $result[$i]->lot_quantity;
        //             array_push($partLot['lots'], $lot);
        //             $partLot['partNo'] = $result[$i]->part_no;
        //             $partLot['partName'] = $result[$i]->part_name;
        //             $partLot['totalQuantity'] = $result[$i]->total_quantity;
        //             $partLot['lotQuantityTotal'] = $lotQuantityTotal;
        //             array_push($invoiceLots, $partLot);
        //         }else{
        //             $partLot['partNo'] = $result[$i-1]->part_no;
        //             $partLot['partName'] = $result[$i-1]->part_name;
        //             $partLot['totalQuantity'] = $result[$i-1]->total_quantity;
        //             $partLot['lotQuantityTotal'] = $lotQuantityTotal;
        //             array_push($invoiceLots, $partLot);
        //             $partLot['lots'] = [];
        //             $partLot['partNo'] = $result[$i]->part_no;
        //             $partLot['partName'] = $result[$i]->part_name;
        //             $partLot['totalQuantity'] = $result[$i]->total_quantity;
        //             $lotQuantityTotal = $result[$i]->lot_quantity;
        //             $partLot['lotQuantityTotal'] = $lotQuantityTotal;
        //             array_push($partLot['lots'], $lot);
        //             array_push($invoiceLots, $partLot);
        //         }
        //     }
        // }

        // return response()->json($invoiceLots);

        $invoiceLots = [];
        $partNoLot = array(
            "partNo" => "",
            "partNameLots" => []
        );
        $partName = array(
            "partName" => "",
            "totalQuantityLots" => []
        );
        $totalQuantity = array(
            "totalQuantity" => 0,
            "actualReceived" => 0,
            "lots" => []
        );

        $samePartNoToPrev = true;
        $samePartNameToPrev = true;
        $sameTotalQuantityToPrev = true;

        for($i = 0; $i < count($result); $i++){
            if($i != 0){
                if(str_replace(' ', '', $result[$i]->part_no) == str_replace(' ', '', $result[$i-1]->part_no)){
                    $samePartNoToPrev = true;
                }else{
                    $samePartNoToPrev = false;
                }

                if(str_replace(' ', '', $result[$i]->part_name) == str_replace(' ', '', $result[$i-1]->part_name)){
                    $samePartNameToPrev = true;
                }else{
                    $samePartNameToPrev = false;
                }

                if(str_replace(' ', '', $result[$i]->total_quantity) == str_replace(' ', '', $result[$i-1]->total_quantity)){
                    $sameTotalQuantityToPrev = true;
                }else{
                    $sameTotalQuantityToPrev = false;
                }
            }

            $lot = (object)[
                'lotNo' => $result[$i]->lot_no,
                'PONo' => $result[$i]->PO_Number,
                'lotQty' => $result[$i]->lot_quantity,
                'boxNo' => $result[$i]->box_number,
                'reject' => $result[$i]->reject,
                'sampleSize' => $result[$i]->sample_size
            ];

            if($i != count($result) - 1){
                if($samePartNoToPrev){
                    if($samePartNameToPrev){
                        if($sameTotalQuantityToPrev){
                            $totalQuantity['actualReceived'] += $result[$i]->lot_quantity;
                            array_push($totalQuantity['lots'], $lot);
                        }else{
                            $totalQuantity['totalQuantity'] = $result[$i - 1]->total_quantity;
                            array_push($partName['totalQuantityLots'], $totalQuantity);

                            $totalQuantity['lots'] = [];

                            $totalQuantity['actualReceived'] = $result[$i]->lot_quantity;
                            array_push($totalQuantity['lots'], $lot);
                        }
                    }else{
                        $partName['partName'] = $result[$i - 1]->part_name;
                        $totalQuantity['totalQuantity'] = $result[$i - 1]->total_quantity;
                        array_push($partName['totalQuantityLots'], $totalQuantity);
                        array_push($partNoLot['partNameLots'], $partName);

                        $partName['totalQuantityLots'] = [];
                        $totalQuantity['lots'] = [];

                        $totalQuantity['actualReceived'] = $result[$i]->lot_quantity;
                        array_push($totalQuantity['lots'], $lot);
                    }
                }else{
                    $partName['partName'] = $result[$i - 1]->part_name;
                    $partNoLot['partNo'] = $result[$i - 1]->part_no;
                    $totalQuantity['totalQuantity'] = $result[$i - 1]->total_quantity;
                    array_push($partName['totalQuantityLots'], $totalQuantity);
                    array_push($partNoLot['partNameLots'], $partName);
                    array_push($invoiceLots, $partNoLot);

                    $partNoLot['partNameLots'] = [];
                    $partName['totalQuantityLots'] = [];
                    $totalQuantity['lots'] = [];

                    $totalQuantity['actualReceived'] = $result[$i]->lot_quantity;
                    array_push($totalQuantity['lots'], $lot);
                }
            }else{
                if($samePartNoToPrev){
                    if($samePartNameToPrev){
                        if($sameTotalQuantityToPrev){
                            $totalQuantity['actualReceived'] += $result[$i]->lot_quantity;
                            $totalQuantity['totalQuantity'] = $result[$i]->total_quantity;
                            array_push($totalQuantity['lots'], $lot);
                            array_push($partName['totalQuantityLots'], $totalQuantity);
                            $partName['partName'] = $result[$i]->part_name;
                            array_push($partNoLot['partNameLots'], $partName);
                            $partNoLot['partNo'] = $result[$i]->part_no;
                            array_push($invoiceLots, $partNoLot);
                        }else{
                            $totalQuantity['totalQuantity'] = $result[$i - 1]->total_quantity;
                            array_push($partName['totalQuantityLots'], $totalQuantity);

                            $totalQuantity['lots'] = [];
                            array_push($totalQuantity['lots'], $lot);
                            $totalQuantity['totalQuantity'] = $result[$i]->total_quantity;
                            $totalQuantity['actualReceived'] = $result[$i]->lot_quantity;
                            array_push($partName['totalQuantityLots'], $totalQuantity);

                            $partName['partName'] = $result[$i]->part_name;
                            array_push($partNoLot['partNameLots'], $partName);

                            $partNoLot['partNo'] = $result[$i]->part_no;
                            array_push($invoiceLots, $partNoLot);
                        }
                    }else{
                        $totalQuantity['totalQuantity'] = $result[$i - 1]->total_quantity;
                        array_push($partName['totalQuantityLots'], $totalQuantity);
                        $partName['partName'] = $result[$i - 1]->part_name;
                        array_push($partNoLot['partNameLots'], $partName);
                        $partNoLot['partNo'] = $result[$i - 1]->part_no;
                        array_push($invoiceLots, $partNoLot);

                        $totalQuantity['lots'] = [];
                        $partName['totalQuantityLots'] = [];
                        array_push($totalQuantity['lots'], $lot);
                        $totalQuantity['totalQuantity'] = $result[$i]->total_quantity;
                        $totalQuantity['actualReceived'] = $result[$i]->lot_quantity;
                        array_push($partName['totalQuantityLots'], $totalQuantity);
                        $partName['partName'] = $result[$i]->part_name;
                        array_push($partNoLot['partNameLots'], $partName);
                        $partNoLot['partNo'] = $result[$i]->part_no;
                        array_push($invoiceLots, $partNoLot);
                    }
                }else{
                    $totalQuantity['totalQuantity'] = $result[$i - 1]->total_quantity;
                    array_push($partName['totalQuantityLots'], $totalQuantity);
                    $partName['partName'] = $result[$i - 1]->part_name;
                    array_push($partNoLot['partNameLots'], $partName);
                    $partNoLot['partNo'] = $result[$i - 1]->part_no;
                    array_push($invoiceLots, $partNoLot);

                    $totalQuantity['lots'] = [];
                    $partName['totalQuantityLots'] = [];
                    $partNoLot['partNameLots'] = [];
                    array_push($totalQuantity['lots'], $lot);
                    $totalQuantity['totalQuantity'] = $result[$i]->total_quantity;
                    $totalQuantity['actualReceived'] = $result[$i]->lot_quantity;
                    array_push($partName['totalQuantityLots'], $totalQuantity);
                    $partName['partName'] = $result[$i]->part_name;
                    array_push($partNoLot['partNameLots'], $partName);
                    $partNoLot['partNo'] = $result[$i]->part_no;
                    array_push($invoiceLots, $partNoLot);
                }
            }
        }

        for ($i = 0; $i < count($invoiceLots); $i++){
            $invoiceLots[$i]['partNoRowSpan'] = 1;
            for ($j = 0; $j < count($invoiceLots[$i]['partNameLots']); $j++){
                $invoiceLots[$i]['partNameLots'][$j]['partNameRowSpan'] = 1;
                $invoiceLots[$i]['partNoRowSpan'] += 1;
                for ($k = 0; $k < count($invoiceLots[$i]['partNameLots'][$j]['totalQuantityLots']); $k++){
                    $invoiceLots[$i]['partNameLots'][$j]['totalQuantityLots'][$k]['totalQuantityRowSpan'] = count($invoiceLots[$i]['partNameLots'][$j]['totalQuantityLots'][$k]['lots']) + 3;
                    $invoiceLots[$i]['partNameLots'][$j]['partNameRowSpan'] += count($invoiceLots[$i]['partNameLots'][$j]['totalQuantityLots'][$k]['lots']) + 3;
                    $invoiceLots[$i]['partNoRowSpan'] += count($invoiceLots[$i]['partNameLots'][$j]['totalQuantityLots'][$k]['lots']) + 3;
                    // if($k > 0){
                    //     $invoiceLots[$i]['partNameLots'][$j]['partNameRowSpan'] += count($invoiceLots[$i]['partNameLots'][$j]['totalQuantityLots'][$k]['lots']) + 3;
                    //     $invoiceLots[$i]['partNoRowSpan'] += count($invoiceLots[$i]['partNameLots'][$j]['totalQuantityLots'][$k]['lots']) + 3;
                    // }else{
                    //     $invoiceLots[$i]['partNameLots'][$j]['partNameRowSpan'] = count($invoiceLots[$i]['partNameLots'][$j]['totalQuantityLots'][$k]['lots']) + 3;
                    //     $invoiceLots[$i]['partNoRowSpan'] = count($invoiceLots[$i]['partNameLots'][$j]['totalQuantityLots'][$k]['lots']) + 3;
                    // }
                }
            }
            
        }

        return response()->json($invoiceLots);
    }

}