<?php

namespace App\Http\Controllers\Api2;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OverallJudgementController extends Controller
{
    public function addOverallJudgement(Request $request){
        $judgement = DB::table('tblOverall_Judgement')
        ->where('MaterialCodeBoxSeqID', '=', $request->matCodeBox)
        ->get();
        
        if(count($judgement) > 0 ){
            $result = $this->updateOverallJudgement($request);
        }else{
            $result = DB::table('tblOverall_Judgement')
            ->insert([
                'invoice_no' => $request->invoice,
                'MaterialCodeBoxSeqID' => $request->matCodeBox,
                'overall_judgement' => $request->judgement
            ]);
        }
        return response()->json($result);
    }

    public function updateOverallJudgement($request){
        return DB::table('tblOverall_Judgement')
        ->where('MaterialCodeBoxSeqID', $request->matCodeBox)
        ->update(['overall_judgement' => $request->judgement]);
    }

    public function addProductionStatus(Request $request){
        $judgement = DB::table('tblOverall_Judgement')
        ->where('MaterialCodeBoxSeqID', '=', $request->matCodeBox)
        ->get();
        
        if(count($judgement) > 0 ){
            $result = $this->updateProductionStatus($request);
        }else{
            $result = DB::table('tblOverall_Judgement')
            ->insert([
                'invoice_no' => $request->invoice,
                'MaterialCodeBoxSeqID' => $request->matCodeBox,
                'Production_Status' => $request->judgement,
                'Return_Date' => date('Y-m-d')
            ]);
        }
        return response()->json($result);
    }

    public function updateProductionStatus($request){
        return DB::table('tblOverall_Judgement')
        ->where('MaterialCodeBoxSeqID', $request->matCodeBox)
        ->update(['Production_Status' => $request->judgement, 'Return_Date' => date('Y-m-d')]);
    }
}
