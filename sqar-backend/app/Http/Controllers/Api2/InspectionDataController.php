<?php

namespace App\Http\Controllers\Api2;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InspectionDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getAll(){
        $result = DB::select('SELECT 
                 A.[id]
                ,A.[invoice_no]
                ,A.[approved]
                ,A.[checked]
                ,A.[prepared]
                ,A.[approved_date]
                ,A.[checked_date]
                ,A.[prepared_date]
                ,A.[temperature]
                ,A.[assembly_line]
                ,A.[part_number]
                ,A.[rohs_compliance]
                ,A.[humidity]
                ,A.[inspected_date]
                ,A.[recieved_date]
                ,A.[supplier]
                ,A.[maker]
                ,A.[inspector]
                ,A.[material_type]
                ,A.[production_type]
                ,A.[inspection_type]
                ,A.[oir]
                ,A.[test_report]
                ,A.[sample_size]
                ,A.[ul_marking]
                ,A.[coc]
                ,A.[partname]
                ,A.[invoicequant]
                ,A.[goodsCode]
                ,A.[MaterialCodeBoxSeqID]
                ,A.[Date]
                ,A.[remarks]
                ,A.[SQAR_Stat]
                ,A.[Inspection_Stat]
                ,A.[PO_Number]
                ,B.[overall_judgement]
                ,B.[Production_Status]
        FROM inspectiondata A
        LEFT JOIN tblOverall_Judgement B ON A.MaterialCodeBoxSeqID = B.MaterialCodeBoxSeqId
        ORDER BY id DESC
        '
        );
        return response()->json($result);
    }

    public function getDistinctInvoiceNo(){
        $result = DB::select('SELECT invoice_no, MAX(id) MAX_ID FROM inspectiondata GROUP BY invoice_no ORDER BY MAX_ID DESC');
        return response()->json($result);
    }

    public function getInvoiceInfo(Request $request){
        $result = DB::select(
            'SELECT TOP 1 [invoice_no]
            ,[assembly_line]
            ,[supplier]
            ,[recieved_date]
            ,[inspected_date]
            FROM [IQCDatabase].[dbo].[inspectiondata]
            WHERE invoice_no = :invoice',
            ['invoice' => $request->invoice]
        );
        return response()->json($result);
    }
}
