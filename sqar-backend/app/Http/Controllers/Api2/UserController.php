<?php

namespace App\Http\Controllers\Api2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserModel;

class UserController extends Controller
{
    public function ChangePassword(Request $request){
        $response = array(
            'success' => false,
            'message' => 'Something went wrong. Please try again.'
        );

        $user = UserModel::CheckPassword($request->empNum, $request->password);

        if($user < 1){
            $response['success'] = false;
            $response['message'] = 'The submitted "Current Password" is incorrect!';
            return response()->json($response);
        }

        $changePassResult = UserModel::ChangePassword($request->empNum, $request->newPassword);

        if($changePassResult){
            $response['success'] = true;
            $response['message'] = 'Your "Password" has been changed!';
        }

        return response()->json($response);
    }
}
