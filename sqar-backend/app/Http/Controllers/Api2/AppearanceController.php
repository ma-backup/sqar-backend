<?php

namespace App\Http\Controllers\Api2;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AppearanceController extends Controller
{
    public function find(Request $request){
        $result = DB::select(
            'SELECT A.[id]
            ,A.[invoice_no]
            ,A.[ig_checkpoints]
            ,A.[instrument_used]
            ,A.[result]
            ,A.[judgement]
            ,A.[remarks]
            ,A.[goodsCode]
            ,A.[defectqty]
            ,A.[defect_enc]
            ,A.[MaterialCodeBoxSeqID]
            ,A.[PO_Number]
            ,A.[SQAR_Stat]
            ,A.[Inspection_Stat]
            ,B.appearance_sample_size
            FROM Appearance_Inspection A
            INNER JOIN SampleSize B
            ON A.MaterialCodeBoxSeqID = B.MaterialCodeBoxSeqID
            WHERE B.id = (SELECT MAX(id) FROM SampleSize WHERE MaterialCodeBoxSeqID = :matCodeBox)
            ORDER BY id',
            ['matCodeBox' => $request->matCodeBox]
        );
        return response()->json($result);
    }
}
