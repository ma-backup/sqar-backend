<?php

namespace App\Http\Controllers\Api2;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FunctionalController extends Controller
{
    public function find(Request $request){
        $result = DB::select(
         'SELECT A.[id]
                ,A.[invoice_no]
                ,A.[checkpoints]
                ,A.[instrument_used]
                ,A.[sample_unit]
                ,A.[sample1]
                ,A.[sample2]
                ,A.[sample3]
                ,A.[sample4]
                ,A.[sample5]
                ,A.[sample6]
                ,A.[sample7]
                ,A.[sample8]
                ,A.[sample9]
                ,A.[sample10]
                ,A.[minimum]
                ,A.[average]
                ,A.[maximum]
                ,A.[lower_spec_limit]
                ,A.[upper_spec_limit]
                ,A.[judgement]
                ,A.[remarks]
                ,A.[goodsCode]
                ,A.[defectqty]
                ,A.[defect_enc]
                ,A.[MaterialCodeBoxSeqID]
                ,A.[Date]
                ,A.[[Remarks_chckpoint]
                ,A.[SQAR_Stat]
                ,A.[Inspection_Stat]
                ,B.[function_sample_size]
                FROM FunctionalCheck A
                INNER JOIN SampleSize B
                ON A.MaterialCodeBoxSeqID = B.MaterialCodeBoxSeqID
                WHERE B.id = (SELECT MAX(id) FROM SampleSize WHERE MaterialCodeBoxSeqID = :matCodeBox)
                ORDER BY id',
                ['matCodeBox' => $request->matCodeBox]
            );
        return response()->json($result);
    }
}
