<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ForSqarProcessController;
use App\Http\Controllers\Api\SqarController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\ImageController;
use App\Http\Controllers\Api\FileController;
use App\Http\Controllers\Api\MS21_Duplicate;
use App\Http\Controllers\Api2\InspectionDataController;
use App\Http\Controllers\Api2\AppearanceController;
use App\Http\Controllers\Api2\LotNumberController;
use App\Http\Controllers\Api2\DimensionalController;
use App\Http\Controllers\Api2\FunctionalController;
use App\Http\Controllers\Api2\OverallJudgementController;
use App\Http\Controllers\Api2\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/for-sqar-process', [ForSqarProcessController::class, 'getForSqarProcess']);
Route::post('/for-sqar-process/search', [ForSqarProcessController::class, 'searchForSqarProcess']);
Route::post('/for-sqar-process/get-rejects', [ForSqarProcessController::class, 'getRejects']);
Route::get('/get-major-defects', [ForSqarProcessController::class, 'getMajorDefects']);
Route::post('/for-sqar-process/get-affected-lot-numbers', [ForSqarProcessController::class, 'getAffectedLotNumbers']);
Route::get('/get-sqar-status-definition', [ForSqarProcessController::class, 'getSqarStatusDefinition']);
Route::get('/get-tech-supp-mngr-supplier', [ForSqarProcessController::class, 'getTechSuppMngrSupplier']);
Route::post('/for-sqar-process/check-critical-ng-checkpoint', [ForSqarProcessController::class, 'checkCriticalNGCheckPoint']);

Route::get('/sqar/count', [SqarController::class, 'countSqar']);
Route::get('/sqar', [SqarController::class, 'getSqars']);
Route::post('/sqar/add', [SqarController::class, 'addSqar']);
Route::post('/sqar/approve', [SqarController::class, 'approveSqar']);

Route::post('/sqar/approve-supplier-material-disposal', [SqarController::class, 'approveSupMatDispo']);
Route::post('/sqar/update-status', [SqarController::class, 'updateSqarStatus']);
Route::post('/sqar/update-status-4', [SqarController::class, 'updateSqarStatusToSupMatDispoReceived']);
Route::post('/sqar/update-image-access-key', [SqarController::class, 'updateImageAccessKey']);
Route::post('/sqar/update-image-access-key-info', [SqarController::class, 'updateImageAccessKeyAndInfo']);
Route::post('/sqar/update-sent-date', [SqarController::class, 'updateSentDate']);
Route::post('/sqar/save-mat-rcca-comments', [SqarController::class, 'saveMatRccaComments']);
Route::post('/sqar/approve-rcca-disposition', [SqarController::class, 'approveRccaDisposition']);
Route::post('/sqar/disapprove-rcca-disposition', [SqarController::class, 'disapproveRccaDisposition']);
Route::post('/sqar/update-supplier-rcca-response-date', [SqarController::class, 'updateSupplierRccaResponseDate']);
//UPDATE RCCA DATE WHEN RCCA WAS ADDED ON DIFFERENT DAY IT WAS RECEIVED
Route::post('/sqar/update-rcca-response-date', [SqarController::class, 'updateRCCAResponseDate']);
Route::post('/sqar/approve-supplier-rcca', [SqarController::class, 'approveSupplierRCCA']);
Route::post('/sqar/approve-delivery', [SqarController::class, 'approveDelivery']);
Route::post('/sqar/save-defects', [SqarController::class, 'saveDefects']);
Route::get('/sqar/get-last-sqar-control-no', [SqarController::class, 'getLastSqarControlNumber']);
Route::post('/sqar/cancel', [SqarController::class, 'cancelSqar']);

Route::post('/sqar/get-image-name', [SqarController::class, 'getImageName']);

Route::post('/login', [LoginController::class, 'Login']);

Route::post('/ms21-get-part-name', [MS21_Duplicate::class, 'findAllPartName']);

//IQC ADMIN ROUTES
Route::get('/iqc-admin/inspection-data', [InspectionDataController::class, 'getAll']);
Route::get('/iqc-admin/inspection-data/get-distinct-invoice', [InspectionDataController::class, 'getDistinctInvoiceNo']);
Route::post('/iqc-admin/inspection-data/get-invoice-info', [InspectionDataController::class, 'getInvoiceInfo']);
Route::post('/iqc-admin/appearance-inspection/find', [AppearanceController::class, 'find']);
Route::post('/iqc-admin/lot-number/find', [LotNumberController::class, 'find']);
Route::post('/iqc-admin/lot-number/find-all', [LotNumberController::class, 'findAll']);
Route::post('/iqc-admin/dimensional-check/find', [DimensionalController::class, 'find']);
Route::post('/iqc-admin/functional-check/find', [FunctionalController::class, 'find']);
Route::post('/iqc-admin/overall-judgement/add', [OverallJudgementController::class, 'addOverallJudgement']);
Route::post('/iqc-admin/overall-judgement/add-production-status', [OverallJudgementController::class, 'addProductionStatus']);
Route::post('ChangePassword', [UserController::class, 'ChangePassword']);

//IMAGES AND FILES ROUTES
Route::post('/image',[ImageController::class, 'imageStore']);
Route::post('/sqar/save-illustrations',[ImageController::class, 'illustrationStore']);
Route::get('image/{path}', [ImageController::class, 'getImage'])->where('path', '.*');

Route::post('/sqar/save-supporting-documents', [FileController::class, 'storeFile']);
Route::get('{path}', [FileController::class, 'retrieveFile'])->where('path', '.*');
Route::post('/sqar/update-supporting-document-information', [FileController::class, 'updateSupportDocument']);

Route::post('/sqar/get-attached-file-information', [FileController::class, 'getFileInformation']);